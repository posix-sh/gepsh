FROM alpine:3.16

RUN apk add shunit2 make

ARG NON_ROOT_USER=gepsh
ARG NON_ROOT_USER_HOME=/home/${NON_ROOT_USER}
RUN adduser \
    -D \
    -h ${NON_ROOT_USER_HOME} \
    -s /bin/sh \
    ${NON_ROOT_USER} ${NON_ROOT_USER}

USER "${NON_ROOT_USER}"
WORKDIR "${NON_ROOT_USER_HOME}"

COPY --chown="${NON_ROOT_USER}:${NON_ROOT_USER}" \
    Docker/Makefile "${NON_ROOT_USER_HOME}"
COPY --chown="${NON_ROOT_USER}:${NON_ROOT_USER}" \
    Docker/tests "${NON_ROOT_USER_HOME}/tests"
COPY --chown="${NON_ROOT_USER}:${NON_ROOT_USER}" \
    src "${NON_ROOT_USER_HOME}/src"

ENV MAKE_TARGET=test

CMD "make" "${MAKE_TARGET}"
