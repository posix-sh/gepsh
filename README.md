[![pipeline status](https://gitlab.com/posix-sh/gepsh/badges/main/pipeline.svg)](https://gitlab.com/posix-sh/gepsh/-/commits/main)

# gepsh

A getopt implementation in posix shell.
