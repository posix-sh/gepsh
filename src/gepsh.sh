#!/bin/sh

readonly __GEPSH_OPT_TYPE_REQARG=1
readonly __GEPSH_OPT_TYPE_NOARG=$((__GEPSH_OPT_TYPE_REQARG + 1))

#
# Print to stdout if the type of option.
#
# Globals:
#   __GEPSH_OPT_TYPE_REQARG
#   __GEPSH_OPT_TYPE_NOARG
#
# Parameters:
#   1: options list; and
#   2: option stripped of prefix dashes.
#
# Outputs:
#   STDOUT: either "${__GEPSH_OPT_TYPE_REQARG}" or "${__GEPSH_OPT_TYPE_NOARG}"
#
__gepsh_opt_type() {
  __tail="${1#*"${2},"}"
  __match="${1%",${__tail}"}"
  __match="${__match##*,}"

  [ "${2}" != "${__match%:}" ] && return 1

  [ -z "${__match#*:}" ] \
    && printf "%d" "${__GEPSH_OPT_TYPE_REQARG}" \
    || printf "%d" "${__GEPSH_OPT_TYPE_NOARG}"
}

#
# Parse given arguments.
#
# Globals:
#   GEPSH_POSIXLY_INCORRECT
#   __GEPSH_OPT_TYPE_REQARG
#   __GEPSH_OPT_TYPE_NOARG
#
# Parameters:
#   1: short options list;
#   2: long options list; and
#   3-: arguments to parse.
#
# Outputs:
#   STDOUT:
#     List of parsed arguments.
#   STDERR:
#     Corresponding error type.
#
gepsh() {
  [ "${#}" -lt 2 ] && {
    printf "gepsh: Arguments: short options list, long options list and arguments." \
      1>&2
    return 1
  }

  __sopts="${1}"
  __lopts="${2}"
  shift 2

  __treat_as_posarg=""
  __optargs=""
  __posargs="--"

  while [ "${#}" -ne 0 ]; do
    case "${__treat_as_posarg}${1}" in
      -[!-])
        __opt_type="$(__gepsh_opt_type "${__sopts}" "${1#-}")" || {
          printf "Invalid option -- \"%s\".\n" "${1}" 1>&2
          return 1
        }

        if [ "${__opt_type}" -eq "${__GEPSH_OPT_TYPE_REQARG}" ]; then
          [ "${#}" -eq 1 ] && {
            printf "Option requires argument -- \"%s\".\n" "${1}" 1>&2
            return 1
          }

          __optargs="${__optargs} \"${1}\" \"${2}\""
          shift
        else
          __optargs="${__optargs} \"${1}\""
        fi
        ;;

      --*=*)
        __lopt="${1%%=*}"
        __lopt="${__lopt#--}"
        __arg="${1##*=}"

        __opt_type="$(__gepsh_opt_type "${__lopts}" "${__lopt}")" || {
          printf "Invalid option -- \"--%s\".\n" "${__lopt}" 1>&2
          return 1
        }

        if [ "${__opt_type}" -eq "${__GEPSH_OPT_TYPE_REQARG}" ]; then
          [ -z "${__arg}" ] && {
            printf "Option requires argument -- \"--%s\".\n" "${__lopt}" 1>&2
            return 1
          }

          __optargs="${__optargs} \"--${__lopt}\" \"${__arg}\""
        elif [ "${__opt_type}" -eq "${__GEPSH_OPT_TYPE_NOARG}" ]; then
          printf "Option takes no argument -- \"--%s\".\n" "${__lopt}" 1>&2
          return 1
        fi
        ;;

      -[!-]*)
        __chars="${1#-}"

        while [ -n "${__chars}" ]; do
          __chars_tail="${__chars#?}"
          __c="${__chars%"${__chars_tail}"}"
          __chars="${__chars#"${__c}"}"

          __opt_type="$(__gepsh_opt_type "${__sopts}" "${__c}")" || {
            printf "Invalid option -- \"-%s\".\n" "${__c}" 1>&2
            return 1
          }

          if [ "${__opt_type}" -eq "${__GEPSH_OPT_TYPE_REQARG}" ]; then
            [ -z "${__chars}" ] && {
              printf "Option requires argument -- \"-%c\".\n" "${__c}" 1>&2
              return 1
            }

            __optargs="${__optargs} \"-${__c}\" \"${__chars}\""

            break
          else
            __optargs="${__optargs} \"-${__c}\""
          fi
        done
        ;;

      --?*)
        __opt_type="$(__gepsh_opt_type "${__lopts}" "${1#--}")" || {
          printf "Invalid option -- \"%s\".\n" "${1}" 1>&2
          return 1
        }

        if [ "${__opt_type}" -eq "${__GEPSH_OPT_TYPE_REQARG}" ]; then
          [ "${#}" -eq 1 ] && {
            printf "Option requires argument -- \"%s\".\n" "${1}" 1>&2
            return 1
          }

          __optargs="${__optargs} \"${1}\" \"${2}\""

          shift
        else
          __optargs="${__optargs} \"${1}\""
        fi
        ;;

      *)
        if [ "${1}" = "--" ] || [ -z "${GEPSH_POSIXLY_INCORRECT}" ]; then
          __treat_as_posarg="1"
        fi

        [ "${1}" != "--" ] && __posargs="${__posargs} \"${1}\""
        ;;

    esac

    shift
  done

  printf "%s %s" "${__optargs}" "${__posargs}"
}
