#!/bin/sh
# file: tests/scripts/gepsh.sh

#
# Shellcheck Global directives
#
# shellcheck source=/dev/null
#
# Suppress warnings about word splitting
# shellcheck disable=SC2086
#

oneTimeSetUp() {
  . "${SRC_FILE}"
}

tearDown() {
  unset sopts_list lopts_list args errmsg errcode parsed_args
}

#
# Give the wrong number of arguments to force an error.
#
testWrongArgCount() {
  gepsh 1>/dev/null 2>&1
  assertFalse \
    " No arguments were passed and the function did not fail." \
    "${?}"

  gepsh "1" 1>/dev/null 2>&1
  assertFalse \
    " One argument was passed and the function did not fail." \
    "${?}"

  return 0
}

#
# Pass invalid options to force an error.
#
testInvOpts() {
  sopts_list="a,b"
  lopts_list="a-long,b-long"
  args="-a --b-long -y"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " Invalid short option was passed, but gepsh did not fail." \
    "${errcode}"

  assertContains \
    " gepsh did fail, but printed the wrong message to stderr." \
    "${errmsg}" \
    "Invalid option --"

  args="-a --b-long --y-long"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " Invalid long option was passed, but gepsh did not fail." \
    "${errcode}"

  assertContains \
    " gepsh did fail, but printed the wrong message to stderr." \
    "${errmsg}" \
    "Invalid option --"

  args="-a --b-long -aby"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " Invalid conjoined option was passed, but gepsh did not fail." \
    "${errcode}"

  assertContains \
    " gepsh did fail, but printed the wrong message to stderr." \
    "${errmsg}" \
    "Invalid option --"

  args="-a --b-long --y-long=arg"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " Invalid long=arg option was passed, but gepsh did not fail." \
    "${errcode}"

  assertContains \
    " gepsh did fail, but printed the wrong message to stderr." \
    "${errmsg}" \
    "Invalid option --"

  return 0
}

#
# Pass option that requires argument without its argument to force an error.
#
testOptTypeFail() {
  sopts_list="a,b:"
  lopts_list="a-long,b-long:"
  args="-a -b"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " reqarg option without argument, but gepsh did not fail." \
    "${errcode}"

  assertContains \
    " reqarg option without arg printed the wrong message to stderr." \
    "${errmsg}" \
    "Option requires argument --"

  args="--a-long=arg -b"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " noarg long=arg opt with argument did not fail." \
    "${errcode}"

  assertContains \
    " noarg long=arg opt with arg printed the wrong message to stderr." \
    "${errmsg}" \
    "Option takes no argument --"

  args="-a --b-long="

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " reqarg long=arg opt without argument did not fail." \
    "${errcode}"

  assertContains \
    " reqarg long=arg opt without arg printed the wrong message to stderr." \
    "${errmsg}" \
    "Option requires argument --"

  args="-ab"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " reqarg conjoined opt passed without argument did not fail." \
    "${errcode}"

  assertContains \
    " reqarg conjoined opt without arg printed the wrong message to stderr." \
    "${errmsg}" \
    "Option requires argument --"

  args="-a --b-long"

  errmsg="$(gepsh "${sopts_list}" "${lopts_list}" ${args} 2>&1)"
  errcode="${?}"

  assertFalse \
    " reqarg long opt without argument, but gepsh did not fail." \
    "${errcode}"

  assertContains \
    " reqarg long opt without arg printed the wrong message to stderr." \
    "${errmsg}" \
    "Option requires argument --"

  return 0
}

#
# Test the printng of short options parsing
#
testShortOptParsePrint() {
  sopts_list="a,b:"
  lopts_list="-"
  set -- -a -b "ArgOfB quoted with spaces" posarg1 pos\ arg\ with\ spaces
  expected=" \"-a\" \"-b\" \"ArgOfB quoted with spaces\" -- \"posarg1\" \"pos arg with spaces\""

  parsed_args="$(gepsh "${sopts_list}" "${lopts_list}" "${@}")"
  errcode="${?}"

  assertTrue \
    "Parsing of short options failed." \
    "${errcode}"

  assertEquals \
    "Printing short options parsing did not match what was expected." \
    "${expected}" "${parsed_args}"

  set -- -- -a -b "ArgOfB quoted with spaces" posarg1 pos\ arg\ with\ spaces
  expected=" -- \"-a\" \"-b\" \"ArgOfB quoted with spaces\" \"posarg1\" \"pos arg with spaces\""

  parsed_args="$(gepsh "${sopts_list}" "${lopts_list}" "${@}")"
  errcode="${?}"

  assertTrue \
    "Parsing of short options (--) failed." \
    "${errcode}"

  assertEquals \
    "Printing short options parsing (--) did not match what was expected." \
    "${expected}" "${parsed_args}"

  set -- posarg_before -a -b "ArgOfB quoted with spaces" posarg1 pos\ arg\ with\ spaces
  expected=" -- \"posarg_before\" \"-a\" \"-b\" \"ArgOfB quoted with spaces\" \"posarg1\" \"pos arg with spaces\""

  parsed_args="$(gepsh "${sopts_list}" "${lopts_list}" "${@}")"
  errcode="${?}"

  assertTrue \
    "Parsing of short options (posarg_before) failed." \
    "${errcode}"

  assertEquals \
    "Printing short options parsing (posarg_before) did not match what was expected." \
    "${expected}" "${parsed_args}"

  set -- posarg_before -a -b "ArgOfB quoted with spaces" posarg1 pos\ arg\ with\ spaces
  expected=" \"-a\" \"-b\" \"ArgOfB quoted with spaces\" -- \"posarg_before\" \"posarg1\" \"pos arg with spaces\""

  parsed_args="$(GEPSH_POSIXLY_INCORRECT="1" gepsh "${sopts_list}" "${lopts_list}" "${@}")"
  errcode="${?}"

  assertTrue \
    "Parsing of short options (GEPSH_POSIXLY_INCORRECT) failed." \
    "${errcode}"

  assertEquals \
    "Printing short options parsing (GEPSH_POSIXLY_INCORRECT) did not match what was expected." \
    "${expected}" "${parsed_args}"
  return 0
}

#
# Test the printng of opt=arg parsing
#
testOptArgParsePrint() {
  sopts_list="-"
  lopts_list="b-long:"
  set -- --b-long="ArgOfB quoted with spaces" posarg1
  expected=" \"--b-long\" \"ArgOfB quoted with spaces\" -- \"posarg1\""

  parsed_args="$(gepsh "${sopts_list}" "${lopts_list}" "${@}")"
  errcode="${?}"

  assertTrue \
    "Parsing of opt=arg options failed." \
    "${errcode}"

  assertEquals \
    "Printing opt=arg parsing did not match what was expected." \
    "${expected}" "${parsed_args}"

  return 0
}

#
# Test the printing of conjoined options
#
testOptConjParsePrint() {
  sopts_list="a,b:"
  lopts_list="-"
  set -- -abArgOfB posarg1
  expected=" \"-a\" \"-b\" \"ArgOfB\" -- \"posarg1\""

  parsed_args="$(gepsh "${sopts_list}" "${lopts_list}" "${@}")"

  errcode="${?}"
  assertTrue \
    "Parsing of conjoined options failed." \
    "${errcode}"

  assertEquals \
    "Printing conjoined opts parsing did not match what was expected." \
    "${expected}" "${parsed_args}"

  return 0
}

#
# Test the printing of long options
#
testLongOptParsePrint() {
  sopts_list="-"
  lopts_list="a-long,b-long:"
  set -- --a-long --b-long ArgOfB posarg1
  expected=" \"--a-long\" \"--b-long\" \"ArgOfB\" -- \"posarg1\""

  parsed_args="$(gepsh "${sopts_list}" "${lopts_list}" "${@}")"
  errcode="${?}"

  assertTrue \
    "Parsing of long options failed." \
    "${errcode}"

  assertEquals \
    "Printing long opts parsing did not match what was expected." \
    "${expected}" "${parsed_args}"

  return 0
}

. shunit2
